#!/bin/bash

if [ ! -f "/data/config/setup.lock" ]
then
  mkdir -p /data/config
  chmod 777 /data/config

  cp /tmp/config/* /data/config/
fi

if [ -f "/data/config/pyload.pid" ]
then
  rm /data/config/pyload.pid
fi

exec python /opt/pyload/pyLoadCore.py
