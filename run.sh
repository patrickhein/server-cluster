#!/bin/bash

ANSIBLE_CONFIG=ansible.cfg ansible-playbook site.yaml --vault-password-file ~/.server-cluster-vault-pass "$@"
