#!/bin/sh

source /netcup_secrets

acme.sh --issue --dns dns_netcup -d "mediathekviewweb.de" -d "*.mediathekviewweb.de"
acme.sh --issue --dns dns_netcup -d "cloudful.de" -d "*.cloudful.de"
acme.sh --issue --dns dns_netcup -d "batrick.de" -d "*.batrick.de"
