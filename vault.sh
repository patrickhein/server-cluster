#!/bin/bash

action="$1"
shift

ansible-vault "$action" --vault-password-file ~/.server-cluster-vault-pass "$@"
